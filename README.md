# ANSIBLE Pritunl VPN server

This repository is to share this Ansible Playbook that allow to create a Pritunl VPN Server following the [**binbash Leverage**](https://leverage.binbash.co/) [Landing Zone](https://leverage.binbash.co/try-leverage/) standards.

## How to

The documentation to use this is in [**binbash Leverage**](https://leverage.binbash.co/) [playbooks page](https://leverage.binbash.co/user-guide/playbooks/).
